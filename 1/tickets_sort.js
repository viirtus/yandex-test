/**
 * Created by Kirill on 26-May-16.
 */

"use strict";

/**
 * Simple function for extending two classes
 * @param Child descendant
 * @param Parent base class
 */
function extend(Child, Parent) {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}

/**
 * Base class for travel card representation.
 * @param departure trip point
 * @param arrival trip point
 * @param seat
 * @constructor
 */
var TravelCard = function (/*string*/ departure, /*string*/ arrival, /*string*/ seat) {
    this._departure = departure;
    this._arrival = arrival;
    this.transportInfo = "anything";
    this.seat = seat;
};

TravelCard.prototype.getDeparturePoint = function () {
    return this._departure;
};

TravelCard.prototype.getArrivalPoint = function () {
    return this._arrival;
};

TravelCard.prototype.setTransportInfo = function(/*string*/ info) {
    this.transportInfo = info;
};

TravelCard.match = function(/*string*/type) {
    return true;
};

/**
 * Factory method for creating basic travel card.
 * @param obj plain javascript object
 * @param classSpecified [* optional]
 * @returns {TravelCard} new instance of travel card
 */
TravelCard.create = function(/*any*/ obj, classSpecified) {
    var clazz = classSpecified || TravelCard;
    var card = new clazz.prototype.constructor(obj.departure, obj.arrival, obj.seat);
    card.transportInfo = obj.transportInfo || card.transportInfo;
    return card;
};

/**
 * Return a string representation of object, which contain all
 * information about travel card transportInfo including start and arrival points and seat info.
 * @returns {string}
 */
TravelCard.prototype.toString = function () {
    var str = "Take " + this.transportInfo + " from " + this._departure + " to " + this._arrival + ". ";
    if (this.seat) {
        str += "Seat: " + this.seat + ". ";
    }
    return str;
};

/**
 * Representation of flight
 */
var AirTravelCard = function () {
    TravelCard.prototype.constructor.apply(this, arguments);
    this.transportInfo = "flight";
    this.baggageDrop = "";
    this.gate = "";
};

extend(AirTravelCard, TravelCard);


AirTravelCard.prototype.toString = function() {
    var str = TravelCard.prototype.toString.call(this);
    if (this.gate) str += "Gate: " + this.gate + ". ";
    if (this.baggageDrop) str += "Baggage drop: " + this.baggageDrop + ". ";
    return str;
};

AirTravelCard.match = function(/*string*/type) {
    return type === "flight";
};

/**
 * Factory method for creating flight travel card.
 * Assign extra fields to instance
 * @param obj plain javascript object
 * @returns {TravelCard} new instance of travel card
 */
AirTravelCard.create = function(/*any*/ obj) {
    var card = TravelCard.create(obj, AirTravelCard);
    card.baggageDrop = obj.baggageDrop || card.baggageDrop;
    card.gate = obj.baggageDrop || card.baggageDrop;
    return card;
};

/**
 * Representation of train
 * @constructor
 */
var TrainTravelCard = function () {
    TravelCard.prototype.constructor.apply(this, arguments);
    this.transportInfo = "train";
};

extend(TrainTravelCard, TravelCard);

TrainTravelCard.match = function(/*string*/type) {
    return type === "train";
};

/**
 * Factory method for creating train travel card.
 * @param obj plain javascript object
 * @returns {TravelCard} new instance of travel card
 */
TrainTravelCard.create = function(/*any*/ obj) {
    return TravelCard.create(obj, TrainTravelCard);
};

/**
 * Representation of bus
 */
var BusTravelCard = function () {
    TravelCard.prototype.constructor.apply(this, arguments);
    this.transportInfo = "bus";
};
extend(BusTravelCard, TravelCard);

BusTravelCard.match = function(/*string*/type) {
    return type === "bus";
};

/**
 * Factory method for creating bus travel card.
 * @param obj plain javascript object
 * @returns {TravelCard} new instance of travel card
 */
BusTravelCard.create = function(/*any*/ obj) {
    return TravelCard.create(obj, BusTravelCard);
};


/**
 * Representation of boat
 * @param departure trip point
 * @param arrival trip point
 * @constructor
 */
var BoatTravelCard = function (/*string*/ departure, /*string*/ arrival) {
    TravelCard.prototype.constructor.call(this, departure, arrival, "no seat specified");
    this.transportInfo = "boat";
};

extend(BoatTravelCard, TravelCard);


BoatTravelCard.match = function(/*string*/type) {
    return type === "boat";
};

/**
 * Factory method for creating boat travel card.
 * @param obj plain javascript object
 * @returns {TravelCard} new instance of travel card
 */
BoatTravelCard.create = function(/*any*/ obj) {
    return TravelCard.create(obj, BoatTravelCard);
};



var TravelCardFactory = {
    classList: [AirTravelCard, TrainTravelCard, BusTravelCard, BoatTravelCard],
    /**
     * Create TravelCard instance matched for type specified in an passed object
     * @param obj to converting
     * @returns {TravelCard} instance or null if obj is not valid
     */
    create: function(/*any*/obj) {
        if (!obj.departure || !obj.arrival) return null;

        var type = obj.type || "";
        for (var i = 0; i < TravelCardFactory.classList.length; i++) {
            var match = TravelCardFactory.classList[i].match(type);
            if (match) return TravelCardFactory.classList[i].create(obj);
        }

        return TravelCard.create(obj);
    },

    /**
     * Create a list of TravelCard object form list of plain objects
     * @param list of plain objects
     * @returns {Array<TravelCard>}
     */
    createList: function(/*Array<any>*/list) {
        var converted = [];
        list.forEach(function(obj) {
            var card = TravelCardFactory.create(obj);
            if (card) converted.push(card);
        });
        return converted;
    }
};

/**
 * Base travel card sort implementation.
 * @param ticketList list of travel cards
 * @constructor
 */
var TicketSort = function (/*Array<TravelCard>*/ ticketList) {
    this.leftMap = {};
    this.rightMap = {};
    this.ticketList = ticketList;
    this.init();
};

/**
 * Fill left and right map using current ticket list
 */
TicketSort.prototype.init = function () {
    var _this = this;
    this.ticketList.forEach(function (t, i) {
        _this.leftMap[t.getDeparturePoint()] = i;
        _this.rightMap[t.getArrivalPoint()] = i;
    });
};

/**
 * Base sort algorithm using filling new array instead of swaping in the passed list.
 * Firstly, need to find a first travel card (departure point).
 * Then, using left map (departure map [departure : TravelCard]) we can
 * take a next card for O(log(n)) time complexity.
 * Totally, algorithm need a O(n*log(n)) time and O(n) memory.
 * @returns {TicketSort} fluent-method
 */
TicketSort.prototype.sort = function() {
    console.log(this.ticketList);
    var _this = this;

    //sorted by trip-chain indexes of list
    var indexes = [];
    var sorted = [];
    var first = this._first();
    if (first === -1) throw new Error("Cycle detected!");
    indexes.push(first);
    
    var lastIndex = first;
    for (var i = 0; i < this.ticketList.length; i++) {
        var currentTicket = this.ticketList[lastIndex];
        var arrival = currentTicket.getArrivalPoint();
        var nextIndex = this.leftMap[arrival];
        if(typeof nextIndex !== 'undefined') {
            indexes.push(nextIndex);
        }
        lastIndex = nextIndex;
    }

    indexes.forEach(function(el) {
        sorted.push(_this.ticketList[el]);
    });
    this.ticketList = sorted;

    return this;
};

/**
 * Get a result of sorting (or, passed list if sort not called yet)
 * @returns {Array.<TravelCard>}
 */
TicketSort.prototype.get = function() {
    return this.ticketList;  
};

TicketSort.prototype.toString = function () {
    return this.ticketList.reduce(function (v1, v2) {
        return v1 + " " + v2;
    })
};

/**
 * Find first (start) card.
 * First card have no the departure point in a other cards.
 * We can find a first for O(n*log(n)) time complexity
 * @returns {number}
 * @protected
 */
TicketSort.prototype._first = function() {
    for (var i = 0; i < this.ticketList.length; i++) {
        var ticket = this.ticketList[i];
        if (!this.rightMap.hasOwnProperty(ticket.getDeparturePoint())) return i;
    }
    return -1;
};

/**
 * Sort implementation for sorting passed list inplace,
 * without crating new array.
 * @constructor
 */
var TicketSortInplace = function () {
    TicketSort.prototype.constructor.apply(this, arguments);
};
extend(TicketSortInplace, TicketSort);

/**
 * Inplace sort method.
 * @returns {TicketSortInplace}
 */
TicketSortInplace.prototype.sort = function() {
    var first = this._first();
    if (first === -1) throw new Error("Cycle detected!");

    this._swap(0, first);
    for (var i = 0; i < this.ticketList.length - 1; i++) {
        var currentTicket = this.ticketList[i];
        var arrival = currentTicket.getArrivalPoint();
        var nextIndex = this.leftMap[arrival];
        this._swap(i + 1, nextIndex);
    }
    return this;
};

/**
 * Swap in a passed list two items by index.
 * Need to update map to.
 * @param i1 first element index fow swap
 * @param i2 second element index fow swap
 * @private
 */
TicketSortInplace.prototype._swap = function(/*number*/i1, /*number*/i2) {
    var tmp = this.ticketList[i1];
    this.leftMap[this.ticketList[i1].getDeparturePoint()] = i2;
    this.leftMap[this.ticketList[i2].getDeparturePoint()] = i1;
    this.ticketList[i1] = this.ticketList[i2];
    this.ticketList[i2] = tmp;
};


