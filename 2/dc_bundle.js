/**
 * Created by Kirill on 28-May-16.
 */

/**
 * Class for "framework" description.
 *
 * @param arg {string | Array<HtmlElement>} can be string selector, in this case
 * entity will fill inner structure using selector and document object as root,
 * or can by array of HTMLElement elements for wrapping.
 * @constructor
 */
var DomEntity = function (arg) {
    this.nativeDomList = [];

    if (typeof arg === 'string') {
        this.nativeDomList = this._find(arg, document);
        this.selector = arg;
    } else if (arg instanceof Array) {
        this.nativeDomList = arg;
    }
};
/**
 * Find all descendants that matched passed selector.
 * @param selector {string} for search
 * @returns {DomEntity} new instance, result of evaluating selector expression
 * on current dom list.
 */
DomEntity.prototype.find = function(/*string*/ selector) {
    var elements = this._find(selector, this.nativeDomList);
    elements.selector = selector;

    return new DomEntity(elements);
};

/**
 * Filter current dom list using selector.
 * @param selector {string} for filtering
 * @returns {DomEntity} new instance, result of filtering
 */
DomEntity.prototype.filter = function(/*string*/selector) {
    var elements = this._filter(selector, this.nativeDomList);
    elements.selector = selector;

    return new DomEntity(elements);
};
/**
 * Add class for each element in current dom list.
 * @param clazz {string} className for adding
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.addClass = function(/*string*/clazz) {
    this.nativeDomList.forEach(function(el) {
        el.classList.add(clazz);
    });
    return this;
};
/**
 * Remove class from each element in current dom list.
 * @param clazz {string} className for removing
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.removeClass = function(/*string*/clazz) {
    this.nativeDomList.forEach(function(el) {
        el.classList.remove(clazz);
    });
    return this;
};

/**
 * Toggle class from each element in current dom list.
 * @param clazz {string} className for toggle
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.toggleClass = function(/*string*/clazz) {
    this.nativeDomList.forEach(function(el) {
        el.classList.toggle(clazz);
    });
    return this;
};
/**
 * Check if all of current dom list elements has a specified class
 * @param clazz {string} fro checking
 * @returns {boolean} true if all has, false otherwise
 */
DomEntity.prototype.hasClass = function(/*string*/clazz) {
    if (this.nativeDomList.length === 0) return false;
    var _this = this;
    return this.nativeDomList.length === this.nativeDomList.filter(function(el) {
            return _this._matchClassList([clazz], el);
        }).length;
};

/**
 * Set css property to each element in current dom list.
 * @param style {object | string} for applying to elements, if string (property name)
 * - value param required.
 * @param value {string} style value
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.css = function(style, /*string*/value) {
    if (typeof style === 'string' && value) {
        var dummy = {};
        dummy[style] = value;
        this._css(dummy, this.nativeDomList);
    }
    else this._css(style, this.nativeDomList);
    return this;
};

/**
 * Set (or append, if flag required) text for all of elements in current dom list.
 * @param text {string} need to be set
 * @param append {boolean} if true - text will be appended to current one
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.text = function(/*string*/text, /*boolean*/append) {
    this.nativeDomList.forEach(function(el) {
        el.innerText = (append ? el.innerText : "") + text;
    });
    return this;
};

/**
 * Set or replace html content of each element in current dom list by passed.
 * @param html {string | DomEntity} plain html text or another instance of DomEntity
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.html = function(/*string*/html) {
    var _this = this;
    this.nativeDomList.forEach(function(el) {
        el.innerHTML = "";
        _this._append(html, [el]);
    });
    return this;
};

/**
 * Append (without replacing) html content of each element in current dom list by passed.
 * @param html {string | DomEntity} plain html text or another instance of DomEntity
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.append = function(html) {
    this._append(html, this.nativeDomList);
    return this;
};

/**
 * Prepend (without replacing) html content of each element in current dom list by passed.
 * @param html {string | DomEntity} plain html text or another instance of DomEntity
 * @returns {DomEntity} this, fluent-method
 */
DomEntity.prototype.prepend = function(html) {
    this._prepend(html, this.nativeDomList);
    return this;
};

/**
 * Append html to passed elements
 * @param html {string | DomEntity} plain html text or another instance of DomEntity
 * @param elements {Array<HTMLElement>} list of elements for appending
 * @private
 */
DomEntity.prototype._append = function(html, /*Array<HTMLElement>*/ elements) {
    elements.forEach(function(el) {
        if (typeof html === "string") {
            el.innerHTML += html;
        }
        if (html instanceof DomEntity) {
            html.nativeDomList.forEach(function(child) {
                el.innerHTML += child.innerHTML;
            });
        }
    });
};

/**
 * Prepend html to passed elements
 * @param html {string | DomEntity} plain html text or another instance of DomEntity
 * @param elements {Array<HTMLElement>} list of elements for prepending
 * @private
 */
DomEntity.prototype._prepend = function(html, /*Array<HTMLElement>*/ elements) {
    elements.forEach(function(el) {
        if (typeof html === "string") {
            el.innerHTML = html + el.innerHTML;
        }
        if (html instanceof DomEntity) {
            html.nativeDomList.forEach(function(child) {
                el.innerHTML = child.innerHTML + el.innerHTML;
            });
        }
    });
};

/**
 * Apply every styles k-v pair to each element of passed list
 * @param styles {object} object that contain css key-value
 * @param elements {Array<HTMLElement>} style value
 * @private
 */
DomEntity.prototype._css = function(styles, /*Array<HTMLElement>*/ elements) {
    elements.forEach(function(el) {
        for (var style in styles) {
            if (styles.hasOwnProperty(style)) {
                el.style[style] = styles[style];
            }
        }
    })
};

/**
 * Filter passed elements using selector.
 * @param selector {string} rules for filtering
 * @param elements {Array<HTMLElement>} list of passed elements for filtering
 * @returns {Array<HTMLElement>} new array of elements matched filter params
 * @private
 */
DomEntity.prototype._filter = function(/*string*/ selector, /*Array<HTMLElement>*/ elements) {
    var queryParams = this._parseSelector(selector);
    var _this = this;

    if (queryParams.id !== null) {
        return [this._getElementById(queryParams.id)];
    }
    
    if (queryParams.tag !== null) {
        elements = elements.filter(function(el) {
            return el.tagName === queryParams.tag; 
        });
    }
    
    if (queryParams.classes.length > 0) {
        elements = elements.filter(function(el) {
            return _this._matchClassList(queryParams.classes, el);
        });
    }
    
    return elements;
};

/**
 * Find all descendants of passed root node using selector rules.
 * @param selector {string} rules for search
 * @param root {HTMLElement | document}
 * @returns {Array<HTMLElement>} new array of elements matched filter params
 * @private
 */
DomEntity.prototype._find = function (selector, root) {
    var queryParams = this._parseSelector(selector);
    var elements = [];
    var _this = this;
    var dirty = false;
    //select by id
    if (queryParams.id !== null) {
        dirty = true;
        return [this._getElementById(queryParams.id)];
    }
    //select by tag name
    if (queryParams.tag !== null) {
        dirty = true;
        elements = this._getElementsByTagName(queryParams.tag, root);
        if (elements.length > 0) root = elements;
    }
    //select by class name
    if (queryParams.classes.length > 0) {
        //has no selected items yet items
        if (!dirty) {
            for (var i = 0; i < queryParams.classes.length; i++) {
                var clazz = queryParams.classes[i];
                var list = elements.concat(this._getElementsByClassName(clazz, root));
                if (elements.length === 0) elements = list;
                else {
                    //array intersection
                    elements = elements.filter(function(e) {
                        return list.indexOf(e) !== -1;
                    })
                }
            }
        } else {
            //has a selected items, need to filter it by class
            elements = elements.filter(function(el) {
                return _this._matchClassList(queryParams.classes, el);
            });
        }
    }

    return elements;
};


/**
 * Check if passed element has all of passed classes.
 * @param classList {Array<string>} for checking
 * @param domElement {HTMLElement} need to checked
 * @returns {boolean} true if domElement has all of classList classes
 * @private
 */
DomEntity.prototype._matchClassList = function(/*Array<string>*/classList, /*HTMLElement*/ domElement) {
    var domClasses = domElement.classList;
    for (var i = 0; i < classList.length; i++) {
        if (!domClasses.contains(classList[i])) return false;
    }
    return true;
};

/**
 * Get elements by class name from root-node.
 * @param clazz {string} class name for searching
 * @param root {Array<HTMLElement> | HTMLElement | document} start node (nodes) for searching
 * If it is array - search results will be aggregated from each of node.
 * @returns {Array<HTMLElement>} converted NodeList into array
 * @private
 */
DomEntity.prototype._getElementsByClassName = function(/*string*/clazz, root) {
    if (root.length > 0) return this._getElementsByClassNameArray(clazz, root);
    return [].slice.call(root.getElementsByClassName(clazz));
};

/**
 * Aggregate result of single searching by class for each of domList element
 * @param clazz {string} class name for searching
 * @param domList {Array<HTMLElement>} list of nodes
 * @return {Array<HTMLElement>} aggregated array
 * @private
 */
DomEntity.prototype._getElementsByClassNameArray = function(/*string*/clazz, /*Array<HTMLElement>*/ domList) {
    return this._aggregateInner(this._getElementsByClassName, clazz, domList);
};

/**
 * Get elements by tag name from root-node.
 * @param name {string} tag name for searching
 * @param root {Array<HTMLElement> | HTMLElement | document} start node (nodes) for searching
 * If it is array - search results will be aggregated from each of node.
 * @returns {Array<HTMLElement>} converted NodeList into array
 * @private
 */
DomEntity.prototype._getElementsByTagName = function (/*string*/name, root) {
    if (root.length > 0) return this._getElementsByTagNameArray(name, root);
    return [].slice.call(root.getElementsByTagName(name));
};

/**
 * Aggregate result of single searching by tag for each of domList element
 * @param name {string} tag name for searching
 * @param domList {Array<HTMLElement>} list of nodes
 * @return {Array<HTMLElement>} aggregated array
 * @private
 */
DomEntity.prototype._getElementsByTagNameArray = function (/*string*/name, /*Array<HTMLElement>*/ domList) {
    return this._aggregateInner(this._getElementsByTagName, name, domList);
};

/**
 * Select element by id
 * @param id {string} for fetching
 * @returns {HTMLElement} matched element
 * @private
 */
DomEntity.prototype._getElementById = function (id) {
    return document.getElementById(id);
};

/**
 * Aggregate results of single execution into list using passed executor method.
 * @param executor {function} method for fetching result
 * @param arg {any} passed into executor arguments
 * @param domList {Array<HTMLElement>} list of nodes for aggregating
 * @returns {Array<HTMLElement>} list of matched elements
 * @private
 */
DomEntity.prototype._aggregateInner = function(executor, arg, domList) {
    var list = [];
    for (var i = 0; i < domList.length; i++) {
        var domElement = domList[i];
        list = list.concat(executor.call(this, arg, domElement));
    }
    return list;
};

/**
 * Convert string selector into map using regexp.
 * Class can be specified as class name followed by dot ('.class').
 * Tag can be specified as tag name ('span').
 * Id can be specified as id followed by sharp ('#id').
 * @param selector {string} plain selector
 * @returns {{classes: Array, id: *, tag: *}} parsed selector
 * @private
 */
DomEntity.prototype._parseSelector = function (selector) {
    var classesRe = /\.([a-z\d\-_]+)/gi;
    var idRe = /#([a-z\d\-_]+)/gi;
    var tagRe = /^([a-z\d\-_]+)/gi;
    var match;
    var classList = [];
    var id = null;
    var tag = null;
    while (match = classesRe.exec(selector)) {
        classList.push(match[1]);
    }
    if (match = idRe.exec(selector)) {
        id = match[1];
    }
    if (match = tagRe.exec(selector)) {
        tag = match[1].toUpperCase();
    }
    return {
        classes: classList,
        id: id,
        tag: tag
    }
};